package minesweep.constant;

/**
 * author: vector.huang
 * date：2016/4/18 19:31
 */
public interface ProtocolConstant {


    short RCP = 10; //远程调用
    short ENTER_MINESWEEP = 102;//进入扫雷游戏
    short BACK_HALL = 103;//返回大厅
    short WAIT_ROOM = 1001;//进入等待房间
    short READY_ROOM = 1002;//在房间里准备
    short GAME_START = 1003;//游戏开始
    short KICK_PLAYER = 1004;//踢出玩家
    short KICKED_BY_SELF = 0;//自己请求退出
    short KICKED_BY_OVERTIME =1;//长时间未准备退出
    short KICKED_BY_OTHER=2;//被玩家踢出
    short EXIT_ROOM = 1005;//请求退出房间
    short SELECT_GIRD = 1006;//选中一个格子
    short GIRD_NOT_OPENED = -2;//格子还没有被揭开
    short CALC_SCORE = 1007;//结算分数
    short GAME_OVER = 1008;//游戏结束
}
