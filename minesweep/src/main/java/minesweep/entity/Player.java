package minesweep.entity;

import org.json.JSONObject;

/**
 * Created by fuatnow on 17/2/9.
 */


class SelInfo
{
    public int row,col;
    public boolean isMarkMine;

    public SelInfo(int row, int col, boolean isMarkMine) {
        this.row = row;
        this.col = col;
        this.isMarkMine = isMarkMine;
    }

    public  JSONObject getInfoJsonObj()
    {
        JSONObject jo = new JSONObject();
        jo.put("row",row);
        jo.put("col",col);
        jo.put("isMarkMine",isMarkMine);
        return jo;
    }
}

public class Player
{
    private int usrID;
    private int netID;
    private float score = 0;
    public static final int STU_NOSEL = 0;
    public static final int STU_HAVESEL = 1;
    public static final int STU_OUTLINE = 2;
    private int status = STU_NOSEL;//0还没有选择  1已经选择过 2掉线
    private boolean isReady = false;
    private int errorSteps = 0;
    private String usrName;
    private String headUrl;
    private SelInfo selInfo= new SelInfo(0,0,false);
    private Room room = null;
    private Long enterTime = 0L;

    public Player(int usrID, int netID, String usrName, String headUrl) {
        this.usrID = usrID;
        this.netID = netID;
        this.usrName = usrName;
        this.headUrl = headUrl;
    }

    public Player(int usrID, int score, int status, int errorSteps, String usrName, String headUrl, SelInfo selInfo) {
        this.usrID = usrID;
        this.score = score;
        this.status = status;
        this.errorSteps = errorSteps;
        this.usrName = usrName;
        this.headUrl = headUrl;
        this.selInfo = selInfo;
    }

    public JSONObject getWaitRoomJsonObj()
    {
        JSONObject jo = new JSONObject();
        jo.put("usrID",usrID);
        jo.put("isReady",isReady);
        jo.put("headUrl",headUrl);
        return jo;
    }

    public  JSONObject getGameStartStateJsonObj()
    {
        JSONObject jo = new JSONObject();
        jo.put("usrID",usrID);
        jo.put("status",status);
        jo.put("score",score);
        jo.put("headUrl",headUrl);
        jo.put("selInfo",selInfo.getInfoJsonObj());
        return jo;
    }

    public JSONObject getCalsScoreStateJsonObj(int deductCoin,float addScore)
    {
        JSONObject jo = new JSONObject();
        jo.put("usrID",usrID);
        jo.put("deductCoin",deductCoin);
        jo.put("addScore",addScore);
        jo.put("score",score);
        jo.put("selInfo",selInfo.getInfoJsonObj());
        return jo;
    }

    public  JSONObject getGameSelStateJsonObj()
    {
        JSONObject jo = new JSONObject();
        jo.put("usrID",usrID);
        jo.put("status",status);
        jo.put("selInfo",selInfo.getInfoJsonObj());
        return jo;
    }


    public int getUsrID() {
        return usrID;
    }

    public void setUsrID(int usrID) {
        this.usrID = usrID;
    }

    public int getNetID() {
        return netID;
    }

    public void setNetID(int netID) {
        this.netID = netID;
    }

    public float getScore() {
        return score;
    }

    public void setScore(float score) {
        this.score = score;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public boolean isReady() {
        return isReady;
    }

    public void setReady(boolean ready) {
        isReady = ready;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public Long getEnterTime() {
        return enterTime;
    }

    public void setEnterTime(Long enterTime) {
        this.enterTime = enterTime;
    }

    public int getErrorSteps() {
        return errorSteps;
    }

    public void setErrorSteps(int errorSteps) {
        this.errorSteps = errorSteps;
    }

    public String getUsrName() {
        return usrName;
    }

    public void setUsrName(String usrName) {
        this.usrName = usrName;
    }

    public String getHeadUrl() {
        return headUrl;
    }

    public void setHeadUrl(String headUrl) {
        this.headUrl = headUrl;
    }

    public SelInfo getSelInfo() {
        return selInfo;
    }

    public void setSelInfo(SelInfo selInfo) {
        this.selInfo = selInfo;
    }
}


