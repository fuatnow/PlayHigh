package minesweep.manager;

import  minesweep.entity.Player;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by fuatnow on 17/2/10.
 */
public class PlayerManager
{
    private static final PlayerManager instance = new PlayerManager();
    private Map<Integer,Player> playersMap = new HashMap<Integer,Player>();


    public static PlayerManager getInstance() {
        return instance;
    }

    public void addPlayer(Player player)
    {
        if(playersMap.containsKey(player.getUsrID()))
        {
            //断开连接

        }
        playersMap.put(player.getUsrID(),player);
    }

    public void dropPlayer(Player player)
    {
        if(player == null)
        {
            return;
        }
        playersMap.remove(player.getUsrID());
    }

    public void dropUserByID(int id)
    {
        Player player = getUserByID(id);
        dropPlayer(player);
    }

    public Player getUserByID(int id)
    {
        if(playersMap.containsKey(id))
        {
            return  playersMap.get(id);
        }
        return  null;
    }

}
