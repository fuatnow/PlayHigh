package minesweep.manager;

import minesweep.entity.Player;
import minesweep.entity.Room;

import java.util.ArrayList;

/**
 * Created by fuatnow on 17/2/10.
 */
public class RoomManager
{
    private static final RoomManager instance = new RoomManager();
    private ArrayList<Room> roomList = new ArrayList<Room>();

    public static RoomManager getInstance()
    {
        return instance;
    }

    public Room getIdleRoom()
    {
        Room idleRoom = null;
        for(int i=0;i<roomList.size();i++)
        {
            Room room = roomList.get(i);
            if(room.isStart() == false && room.getMembersList().size() < 7)
            {
                idleRoom = room;
                break;
            }
        }
        if(idleRoom == null)
        {
            idleRoom = new Room();
            roomList.add(idleRoom);
        }
        return idleRoom;
    }

    public Player getPlayer(int netID)
    {
        for(int i=0;i<roomList.size();i++)
        {
            Room tmpRoom = roomList.get(i);
            Player tmpPlayer = tmpRoom.getPlayer(netID);
            if(tmpPlayer != null)
            {
                return tmpPlayer;
            }
        }
        return null;
    }
}
