package minesweep.manager;

import io.netty.channel.ChannelHandlerContext;
import minesweep.entity.Player;
import minesweep.entity.Room;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import static minesweep.constant.ProtocolConstant.*;

/**
 * Created by fuatnow on 17/1/10.
 */
public class MessageManager
{
    private static final Logger logger = LoggerFactory.getLogger(MessageManager.class);
    private static MessageManager instance;
    private static Lock lock = new ReentrantLock();
    public static MessageManager getInstance(){
        if(instance == null){
            lock.lock();
            if(instance == null){
                instance = new MessageManager();
            }
            lock.unlock();
            lock = null;
        }
        return instance;
    }

    public void processMessage(ChannelHandlerContext ctx , String msg )
    {
        try
        {
            JSONObject jo = new JSONObject(msg);
            int cmdID = jo.getInt("cmdID");
            switch (cmdID)
            {
                case ENTER_MINESWEEP:
                    enterGame(jo.getJSONObject("data"));
                    break;
                case READY_ROOM:
                    readyRoom(jo.getJSONObject("data"));
                    break;
                case KICK_PLAYER:
                    kickPlayer(KICKED_BY_OTHER,jo.getJSONObject("data"));
                    break;
                case EXIT_ROOM:
                    kickPlayer(KICKED_BY_SELF,jo.getJSONObject("data"));
                    break;
                case SELECT_GIRD:
                    selectGird(jo.getJSONObject("data"));
                    break;
                default:
                    break;
            }
        }
        catch (JSONException e)
        {
            System.out.print(e.getStackTrace());
        }
    }

   void enterGame(JSONObject jo)
    {
        //排队
        int usrID = jo.getInt("usrID");
        int netID = jo.getInt("netID");
        String  headUrl = jo.getString("headUrl");
        String usrName = jo.getString("usrName");
        Player player = new Player(usrID,netID,usrName,headUrl);
        Room room = RoomManager.getInstance().getIdleRoom();
        room.addPlayer(player);
    }

    void readyRoom(JSONObject jo)
    {
        int netID = jo.getInt("netID");
        boolean isReady = jo.getBoolean("isReady");
        Player p = RoomManager.getInstance().getPlayer(netID);
        p.setReady(isReady);
        Room room = p.getRoom();
        room.sendSomeOnceReady();
    }

    void kickPlayer(int reason ,JSONObject jo)
    {
        int netID = jo.getInt("netID");
        int usrID = jo.getInt("usrID");
        Player p = RoomManager.getInstance().getPlayer(netID);
        Room room = p.getRoom();
        room.kickPlayer(room.getPlayerByUsrID(usrID),reason,p.getUsrName());
    }

    void selectGird(JSONObject jo)
    {
        int netID = jo.getInt("netID");
        int row = jo.getInt("row");
        int col = jo.getInt("col");
        boolean isMarkMine = jo.getBoolean("isMarkMine");
        Player p = RoomManager.getInstance().getPlayer(netID);
        Room room = p.getRoom();
        room.sendPlayerSelGird(p,row,col,isMarkMine);
    }
}
