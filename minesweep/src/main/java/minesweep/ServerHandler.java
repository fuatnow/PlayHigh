package minesweep;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import minesweep.entity.JsonMessage;
import minesweep.manager.MessageManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by fuatnow on 17/2/11.
 */
public class ServerHandler extends SimpleChannelInboundHandler<String> {

    private static final Logger logger = LoggerFactory.getLogger(ServerHandler.class);
    private static ChannelHandlerContext _mineSweepGateConnection = null;

    public static ChannelHandlerContext getMineSweepGateConnection() {
        return _mineSweepGateConnection;
    }
    public void channelActive(ChannelHandlerContext ctx) throws Exception
    {
        _mineSweepGateConnection = ctx;
        JsonMessage jmsg = new JsonMessage(102);
        String rpcConStr = jmsg.toString();
        ctx.writeAndFlush(rpcConStr);
    }

    @Override
    protected void channelRead0(ChannelHandlerContext channelHandlerContext, String message) throws Exception
    {
//        Internal.GTransfer gtf = (Internal.GTransfer) message;
//
//        Message cmd = ParseMap.getMessage(gtf.getPtoNum(), gtf.getMsg().toByteArray());
//
//        ByteBuf out = Utils.pack2Client(cmd);
//
//        ClientConnectionMap.getClientConnection(gtf.getNetId()).getCtx().writeAndFlush(out);
//        String msg = (String) message;
        logger.info(message);
        MessageManager.getInstance().processMessage(channelHandlerContext,message);
    }
}
