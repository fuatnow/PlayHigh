package com.fuatnow.game.serviece;

import com.fuatnow.game.entity.User;

import java.util.List;
import java.util.Map;

/**
 * Created by fuatnow on 17/1/8.
 */
public class RequestService
{
    public  static User reigsterClient(Map<String, List<String>> params)
    {
        if(params.containsKey("usr") && params.containsKey("pwd"))
        {
            try{
                String usrName = params.get("usr").get(0);
                String pwdName = params.get("pwd").get(0);
                User reg = new User();
                reg.setUsrName(usrName);
                reg.setPassword(pwdName);
                return  reg;
            }
            catch (Exception e)
            {
                return  null;
            }
        }
        return  null;
    }
}
