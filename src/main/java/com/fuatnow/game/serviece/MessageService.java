package com.fuatnow.game.serviece;

import com.fuatnow.game.entity.Response;

/**
 * Created by fuatnow on 17/1/8.
 */
public class MessageService
{
    public static Response sendMessage(int cmdId, String message) {
        Response res = new Response();
        res.getData().put("cmdID", cmdId);
        res.getData().put("message", message);
        res.getData().put("ts", System.currentTimeMillis());// 返回毫秒数
        return res;
    }

    public  static  Response registerUser(int cmdId, int userId)
    {
        Response res = new Response();
        res.getData().put("cmdID", cmdId);
        res.getData().put("usrID", userId);
        return res;
    }
}
