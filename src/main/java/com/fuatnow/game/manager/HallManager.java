package com.fuatnow.game.manager;

import com.fuatnow.game.database.DBHelper;
import com.fuatnow.game.entity.JsonMessage;
import com.fuatnow.game.entity.User;
import org.json.JSONArray;
import org.json.JSONObject;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by fuatnow on 17/2/18.
 */
public class HallManager
{
    private static final HallManager instance = new HallManager();

    private  HallManager()
    {

    }

    public static HallManager getInstance()
    {
        return instance;
    }

    public User getUserInfo(int usrID,String pwd)
    {
        String sqlStr = "select * from  UserInfo where  usrID = "+usrID+" and pwd = '"
                + pwd+"';";
        ResultSet res = DBHelper.getInstance().getResultSet(sqlStr);
        try {
            if (res != null && res.next())
            {
                int coinNum = res.getInt("coin");
                int level = res.getInt("level");
                String headUrl = res.getString("photoID");
                String usrName = res.getString("usrName");
                User user = new User();
                user.setUsrID(usrID);
                user.setUsrName(usrName);
                user.setPhotoUrl(headUrl);
                user.setLevelNum(level);
                user.setCoinNum(coinNum);
                user.setPassword(pwd);
                return user;
            }
        }catch (SQLException e)
        {

        }
        return  null;
    }


    public JSONObject getHallInfo(User user)
    {
        if(user == null) return null;
        JsonMessage jo = new JsonMessage();
        JSONObject userInfo = new JSONObject();
        userInfo.put("usrID",user.getUsrID());
        userInfo.put("coin",user.getCoinNum());
        userInfo.put("usrName",user.getUsrName());
        userInfo.put("level",user.getLevelNum());
        userInfo.put("headUrl",user.getPhotoUrl());
        jo.put("usrInfo",userInfo);
        JSONArray gameHallInfo = getGameHallInfo();
        jo.put("gameHall",gameHallInfo);
        jo.put(JsonMessage.ERROR_CODE_KEY,0);
        return jo.getRootObj();
    }

    JSONArray getGameHallInfo()
    {
        JSONArray gameArr = new JSONArray();

        JSONObject saoleiObj = new JSONObject();
        saoleiObj.put("name","minesweep");
        saoleiObj.put("onlineNum",2000+Math.random()*100);
        gameArr.put(saoleiObj);

        JSONObject chuiniuObj = new JSONObject();
        chuiniuObj.put("name","chuiniu");
        chuiniuObj.put("onlineNum",2000+Math.random()*100);
        gameArr.put(chuiniuObj);
        return gameArr;
    }

}
