package com.fuatnow.game.manager;

/**
 * Created by fuatnow on 17/1/18.
 */

import com.fuatnow.game.entity.User;
import io.netty.channel.ChannelHandlerContext;
import io.netty.util.AttributeKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.atomic.AtomicInteger;

public class ClientConnection {
    private static final Logger logger = LoggerFactory.getLogger(ClientConnection.class);

    private static final AtomicInteger netidGenerator = new AtomicInteger(0);
    public static AttributeKey<Integer> NETID = AttributeKey.valueOf("netid");

    private User _user;
    private int _netId;
    private ChannelHandlerContext _ctx = null;
    private ChannelHandlerContext nodeCtx = null;

    ClientConnection(ChannelHandlerContext c) {
        _netId = netidGenerator.incrementAndGet();
        _ctx = c;
        _ctx.attr(ClientConnection.NETID).set(_netId);
    }


    public int getNetID() {
        return _netId;
    }

    public User getUser() {
        return _user;
    }

    public void setUser(User _user) {
        this._user = _user;
    }

    public int getUserID()
    {
        return _user.getUsrID();
    }

    public void readUserIdFromDB() {

    }

    public void setCtx(ChannelHandlerContext ctx) {_ctx = ctx;}
    public ChannelHandlerContext getCtx() {
        return _ctx;
    }

    public ChannelHandlerContext getNodeCtx() {
        return nodeCtx;
    }

    public void setNodeCtx(ChannelHandlerContext nodeCtx) {
        this.nodeCtx = nodeCtx;
    }
}