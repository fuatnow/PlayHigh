package com.fuatnow.game.entity;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by fuatnow on 17/1/8.
 */
public class User
{
    private String usrName;
    private String password;
    private int usrID;
    private String photoUrl;
    private boolean isMen;
    private Date birthDate;
    private Date lastLogin;
    private String telephone;
    private String emailAddress;
    private int coinNum;
    private int levelNum;

    public User()
    {
        this.usrID = -1;
    }

    public User(String usrName, String password, int usrID, String photoUrl, boolean isMen, Date birthDate, Date lastLogin, String telephone, String emailAddress) {
        this.usrName = usrName;
        this.password = password;
        this.usrID = usrID;
        this.photoUrl = photoUrl;
        this.isMen = isMen;
        this.birthDate = birthDate;
        this.lastLogin = lastLogin;
        this.telephone = telephone;
        this.emailAddress = emailAddress;
    }


    @Override
    public String toString() {
        return "User{" +
                "usrName='" + usrName + '\'' +
                ", password='" + password + '\'' +
                ", usrID=" + usrID +
                ", photoId='" + photoUrl + '\'' +
                ", isMen=" + isMen +
                ", birthDate=" + birthDate +
                ", lastLogin=" + lastLogin +
                ", telephone='" + telephone + '\'' +
                ", emailAddress='" + emailAddress + '\'' +
                '}';
    }

    public String getUsrName() {
        return usrName;
    }

    public void setUsrName(String usrName) {
        this.usrName = usrName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getUsrID() {
        return usrID;
    }

    public void setUsrID(int usrID) {
        this.usrID = usrID;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public boolean isMen() {
        return isMen;
    }

    public void setMen(boolean men) {
        isMen = men;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }
//
//    public Date getLastLogin() {
//        return lastLogin;
//    }

    public  String getLastLogin()
    {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date now = new Date();
        return sdf.format(now).toString();
    }

    public void setLastLogin(Date lastLogin) {
        this.lastLogin = lastLogin;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public int getCoinNum() {
        return coinNum;
    }

    public void setCoinNum(int coinNum) {
        this.coinNum = coinNum;
    }

    public int getLevelNum() {
        return levelNum;
    }

    public void setLevelNum(int levelNum) {
        this.levelNum = levelNum;
    }
}
