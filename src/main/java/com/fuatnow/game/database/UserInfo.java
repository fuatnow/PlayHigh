package com.fuatnow.game.database;

import com.fuatnow.game.entity.User;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;

import java.sql.ResultSet;
import java.sql.SQLException;


/**
 * Created by fuatnow on 17/1/9.
 */
public class UserInfo extends OperationDB
{
    private ChannelHandlerContext ctx = null;
    private FullHttpRequest req = null;

    public  UserInfo(dbExecuteStateListener db)
    {
        this.m_dbListener = db;
    }

    public  UserInfo()
    {

    }

    public  void register(User user)
    {
        String sql="insert into UserInfo(usrName,pwd,lastlogin) values('"+user.getUsrName()+"','"
                +user.getPassword()+"','"+user.getLastLogin()+"');";
        try{
            this.setMysql(sql);
            String selSql = "select max(usrID) from UserInfo;";
            ResultSet res = DBHelper.getInstance().getResultSet(selSql);
            if(res != null && res.next())
            {
                int usrID = res.getInt(1);
                user.setUsrID(usrID);
//                ResultSetMetaData m=res.getMetaData();
//                int columns=m.getColumnCount();
//                //显示列,表格的表头
//                for(int i=1;i<=columns;i++)
//                {
//                    System.out.print(m.getColumnName(i));
//                    System.out.print("\t\t");
//                }
//
//                System.out.println();
//                //显示表格内容
//                while(res.next())
//                {
//                    for(int i=1;i<=columns;i++)
//                    {
//                        System.out.print(res.getInt(i));
//                        System.out.print("\t\t");
//                    }
//                    System.out.println();
//                }
            }
        }
        catch (Exception e)
        {
            user.setUsrID(-1);
            System.err.print(e.getStackTrace());
        }

    }

    public boolean loginSuc(User usr)
    {
        String sqlStr = "select * from  UserInfo where  usrID = "+usr.getUsrID()+" and pwd = "
                + usr.getPassword()+";";
        ResultSet res = DBHelper.getInstance().getResultSet(sqlStr);
        try {
            if (res != null && res.next()) {
                return  true;
            }
        }catch (SQLException e)
        {
            return  false;
        }
        return  false;
    }

    @Override
    public void processage() {

    }

    @Override
    public void insert(User user) {

        String sql="insert into UserInfo(usrName,pwd,lastlogin) values('"+user.getUsrName()+"','"
                +user.getPassword()+"','"+user.getLastLogin()+"');";
        this.setMysql(sql);
    }

    @Override
    public void delete(User user) {

    }

    @Override
    public void update(User user) {

    }

    @Override
    protected void setMysql(String mysql) {
        super.setMysql(mysql);
    }

    @Override
    public ResultSet select(int qq, String desired) {
        return null;
    }

    public ChannelHandlerContext getCtx() {
        return ctx;
    }

    public void setCtx(ChannelHandlerContext ctx) {
        this.ctx = ctx;
    }

    public FullHttpRequest getReq() {
        return req;
    }

    public void setReq(FullHttpRequest req) {
        this.req = req;
    }
}
