package com.fuatnow.game.database;

import com.fuatnow.game.entity.User;

import java.sql.ResultSet;

/**
 * Created by fuatnow on 17/1/8.
 */
public abstract class OperationDB
{
    public abstract void insert(User user);
    public abstract void delete(User user);
    public abstract void update(User user);
    protected void setMysql(String mysql)
    {
        boolean exSuc = DBHelper.getInstance().setSql(mysql);
        if(exSuc == false && m_dbListener != null)
        {
            m_dbListener.executeFailed(-1,"error");
        }
    }
    public abstract ResultSet select(int qq, String desired);
    protected dbExecuteStateListener m_dbListener;
    public abstract void processage();
}
