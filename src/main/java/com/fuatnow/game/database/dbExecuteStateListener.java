package com.fuatnow.game.database;

/**
 * Created by fuatnow on 17/1/9.
 */


interface dbExecuteCallBack
{
    public void executeSuccess(String msg);
    public void executeFailed(int errorCode,String msg);
}

public class dbExecuteStateListener implements dbExecuteCallBack
{
    public void executeSuccess(String msg){

    }
    public void executeFailed(int errorCode,String msg){

    }
}


