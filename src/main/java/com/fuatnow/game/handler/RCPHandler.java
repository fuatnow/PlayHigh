package com.fuatnow.game.handler;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;

/**
 * Created by fuatnow on 17/2/14.
 */
public class RCPHandler extends MsgHander
{
    private boolean isNodeToClient = true;
    public RCPHandler(ChannelHandlerContext ctx, String msg) {
        super(ctx, msg);
    }

    @Override
    public void processMsg() throws Exception {
        if(isNodeToClient) {
            _ctx.writeAndFlush(new TextWebSocketFrame(_msg));
        }
        else
        {
            _ctx.writeAndFlush(_msg);
        }
    }

    public boolean isNodeToClient() {
        return isNodeToClient;
    }

    public void setNodeToClient(boolean nodeToClient) {
        isNodeToClient = nodeToClient;
    }
}
