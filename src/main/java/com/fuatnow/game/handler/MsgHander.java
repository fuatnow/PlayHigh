package com.fuatnow.game.handler;

import io.netty.channel.ChannelHandlerContext;

/**
 * Created by fuatnow on 17/1/18.
 */
public abstract class MsgHander
{
    protected String _msg;
    protected ChannelHandlerContext _ctx;
    public abstract void processMsg() throws Exception;

    protected MsgHander(ChannelHandlerContext ctx, String msg)
    {
        _ctx = ctx;
        _msg = msg;
    }
}
