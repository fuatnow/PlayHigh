package com.fuatnow.game.handler;

import com.fuatnow.game.database.DBHelper;
import com.fuatnow.game.entity.JsonMessage;
import com.fuatnow.game.entity.User;
import com.fuatnow.game.manager.ClientConnectManager;
import com.fuatnow.game.manager.ClientConnection;
import com.fuatnow.game.manager.HallManager;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import org.json.JSONObject;

import java.sql.ResultSet;
import java.sql.SQLException;

import static com.fuatnow.game.constant.ProtocolConstant.USER_LOGIN;

/**
 * Created by fuatnow on 17/1/18.
 */
public class LoginHandler extends MsgHander
{
    public LoginHandler(ChannelHandlerContext ctx, String msg) {
        super(ctx, msg);
    }

    @Override
    public void processMsg() throws Exception {
        JSONObject jo = new JSONObject(_msg);
        int cmdID = jo.getInt("cmdID");
        JSONObject dataObj = jo.getJSONObject("data");
        int usrID = dataObj.getInt("usrID");
        String pwd = dataObj.getString("pwd");
        String sqlStr = "select * from  UserInfo where  usrID = "+usrID+" and pwd = '"
                + pwd+"';";
        ResultSet res = DBHelper.getInstance().getResultSet(sqlStr);
        try {
            if (res != null && res.next())
            {
//                int coinNum = res.getInt("coin");
//                int level = res.getInt("level");
//                String headUrl = res.getString("photoID");
//                String usrName = res.getString("usrName");
//                JSONObject userInfo = new JSONObject();
//                userInfo.put("usrID",usrID);
//                userInfo.put("coin",coinNum);
//                userInfo.put("usrName",usrName);
//                userInfo.put("level",level);
//                userInfo.put("headUrl",headUrl);
//                jMsg.put("usrInfo",userInfo);
//                JSONArray gameHallInfo = HallManager.getInstance().getGameHallInfo();
//                jMsg.put("gameHall",gameHallInfo);
//                jMsg.put(JsonMessage.ERROR_CODE_KEY,0);
//                String jsonStr = jMsg.toString();
//                _ctx.writeAndFlush(new TextWebSocketFrame(jsonStr));
                User user = HallManager.getInstance().getUserInfo(usrID,pwd);
                if(user != null)
                {
                    JSONObject hallJo = HallManager.getInstance().getHallInfo(user);
                    hallJo.put("cmdID",USER_LOGIN);
                    String jsonStr = hallJo.toString();
                    _ctx.writeAndFlush(new TextWebSocketFrame(jsonStr));

                    ClientConnection conn = ClientConnectManager.addClientConnection(_ctx);
                    if(conn != null)
                    {
                        conn.setUser(user);
                        ClientConnectManager.registerUserid(user,conn.getNetID());
                    }
                }

//                User user =  new User();
//                user.setUsrID(usrID);
//                user.setUsrName(usrName);
//                user.setPhotoUrl(headUrl);
//                ClientConnection conn = ClientConnectManager.addClientConnection(_ctx);
//                if(conn != null)
//                {
//                    conn.setUser(user);
//                    ClientConnectManager.registerUserid(user,conn.getNetID());
//                }
                return;
            }
        }catch (SQLException e)
        {

        }
        JsonMessage jMsg = new JsonMessage(USER_LOGIN);
        jMsg.put(JsonMessage.ERROR_CODE_KEY,1);
        jMsg.put(JsonMessage.ERROR_MSG_KEY,"unknown");
        String jsonStr = jMsg.toString();
        _ctx.writeAndFlush(new TextWebSocketFrame(jsonStr));
    }
}
